package com.example.plugins.tutorial.jira.mailhandlerdemo;

public interface MyPluginComponent
{
    String getName();
}